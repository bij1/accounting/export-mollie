import * as R from 'ramda';

export const log = console.log.bind(console);

export const mapKeys: (fn: (k: string) => string) => (o?: {[k: string]: any}) => {[k: string]: any} = (fn: (k: string) => string) =>
    R.pipe(R.defaultTo({}), Object.entries, R.map(R.adjust(0, fn)), Object.fromEntries);

export const prepend: (k: string) => (k: string) => string = R.curry((a,b) => a+b);
