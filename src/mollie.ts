"use strict";
import { List } from "@mollie/api-client";

export const mollieKeys = {
    leden: process.env.MOLLIE_API_KEY_LEDEN || '',
};

export async function listAll<T>(list$: Promise<List<T>>, ms: number = 1000): Promise<T[]> {
    const list = await list$;
    const { count, nextPage, links } = list;
    console.log('.');
    await new Promise(resolve => setTimeout(resolve, ms));
    const rest = await (nextPage ? listAll(nextPage()) : Promise.resolve([]));
    return [...list, ...rest];
}
