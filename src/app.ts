"use strict";
import { writeFileSync } from 'fs';
import { unparse } from 'papaparse';
import { createMollieClient, MollieClient } from '@mollie/api-client';
import { log, mapKeys, prepend } from './utils';
import { mollieKeys, listAll } from './mollie';
import assert from 'assert';

type PaymentObj = { [k: string]: any };

async function main() {
    const apiKey = mollieKeys.leden;
    const mollie = createMollieClient({ apiKey });
    const customers = await exportMollieCustomers(mollie);
    const customerObj = Object.fromEntries(customers.map(({ id, ...rest }) => [id, rest]));
    const payments = await exportMolliePayments(mollie, customerObj);
    const paymentObj: PaymentObj = Object.fromEntries(payments.map(({ id, ...rest }) => [id, rest]));
    assert(apiKey.slice(0,5) == 'live_');
    const refunds = await exportMollieRefunds(mollie, paymentObj);
    const chargebacks = await exportMollieChargebacks(mollie, paymentObj);
}

async function exportMollieCustomers(mollie: MollieClient): Promise<{[k: string]: any}[]> {
    const page$ = mollie.customers.list();
    const list = (await listAll(page$, 500))
        // .map(({ id, name, email, createdAt }) => ({ id, name, email, createdAt }));
    writeFileSync('./data/customers.csv', unparse(list));
    return list;
}

async function exportMollieRefunds(mollie: MollieClient, paymentObj: PaymentObj): Promise<{[k: string]: any}[]> {
    const page$ = mollie.refunds.list();
    const list = (await listAll(page$, 500))
        .map(({ resource, id, amount, status, createdAt, description, metadata, paymentId, settlementId, settlementAmount, _links, lines }) => ({ resource, id, status, createdAt, description, metadata, paymentId, settlementId, lines, ...mapKeys(prepend('amount.'))(amount), ...mapKeys(prepend('settlementAmount.'))(settlementAmount)/*, ...mapKeys(prepend('_links.'))(_links)*/, ...mapKeys(prepend('payment.'))(paymentObj[paymentId || '']) }));
    writeFileSync('./data/refunds.csv', unparse(list));
    return list;
}

async function exportMollieChargebacks(mollie: MollieClient, paymentObj: PaymentObj): Promise<{[k: string]: any}[]> {
    const page$ = mollie.chargebacks.list();
    const list = (await listAll(page$, 500))
        .map(({ resource, id, amount, createdAt, reason, paymentId, settlementAmount, _links }) => ({ resource, id, createdAt, paymentId, ...mapKeys(prepend('reason.'))(reason), ...mapKeys(prepend('amount.'))(amount), ...mapKeys(prepend('settlementAmount.'))(settlementAmount)/*, ...mapKeys(prepend('_links.'))(_links)*/, ...mapKeys(prepend('payment.'))(paymentObj[paymentId || '']) }));
    writeFileSync('./data/chargebacks.csv', unparse(list));
    return list;
}

async function exportMolliePayments(mollie: MollieClient, customerObj: { [id: string]: { name: string, email: string, createdAt: string } }): Promise<{[k: string]: any}[]> {
    const page$ = mollie.payments.list();
    const list = (await listAll(page$, 500))
    // const list = Array.from(await page$)
        .map(({ resource, id, createdAt, paidAt, description, method, status, isCancelable, sequenceType, profileId, customerId, mandateId, details, amount, settlementAmount, amountRefunded, amountRemaining }) => ({ resource, id, createdAt, paidAt, description, method, status, isCancelable, sequenceType, profileId, customerId, mandateId, ...mapKeys(prepend('details.'))(details), ...mapKeys(prepend('amount.'))(amount), ...mapKeys(prepend('settlementAmount.'))(settlementAmount), ...mapKeys(prepend('amountRefunded.'))(amountRefunded), ...mapKeys(prepend('amountRemaining.'))(amountRemaining), ...mapKeys(prepend('customer.'))(customerObj[customerId || '']) }));
    writeFileSync('./data/payments.csv', unparse(list));
    return list;
}

main();
