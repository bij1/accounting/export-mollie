BIJ1 Compliance Facilitator App
===============================

### Prerequisites

- [Yarn](https://yarnpkg.com/)

### Usage

`yarn install` installs the dependencies

set the API keys for [Mollie](https://www.mollie.com/dashboard/org_3678554/developers/api-keys):
```bash
export NODE_OPTIONS=--experimental-vm-modules npx jest
export MOLLIE_API_KEY_LEDEN=<MY_KEY>
```

`yarn start` launches the app

`yarn run test` launches the tests (using JSHint and Jest)

`yarn run dev` watches src/ for changes and runs test when anything does change

$(yarn bin)/jest -- tests/mollie.test.ts
