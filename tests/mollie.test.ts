import { createMollieClient } from '@mollie/api-client';
import { mollieKeys, listAll } from '../src/mollie';

describe('mollie', () => {

  const mollie = createMollieClient({ apiKey: mollieKeys.leden });

  it('can fetch a page of payments', async () => {
    const list = await mollie.payments.list();
    expect(list.length).toEqual(50);
  });

  xit('can paginate customers', async () => {
    const list = await listAll(mollie.customers.list());
    expect(list.length).toBeGreaterThan(50);
  });

})
